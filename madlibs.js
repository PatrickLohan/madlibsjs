function madPrompts () {
    // Setting up countdown variable
    var questions = 5;
    var questionsLeft = '[' + questions + ' questions left.]';

    // Prompting user for various words and placing the input into variables
    var name = prompt("Choose a name, adventurer! " + questionsLeft);
    questions -= 1;
    questionsLeft = '[' + questions + ' questions left.]';
    
    var adverb = prompt("Choose a past-tense adverb. " + questionsLeft);
    questions -= 1;
    questionsLeft = '[' + questions + ' questions left.]';
    
    var place = prompt("Choose a place or country. " + questionsLeft);
    questions -= 1;
    questionsLeft = '[' + questions + ' questions left.]';

    var adjective = prompt("Choose an adjective. " + questionsLeft);
    questions -= 1;
    questionsLeft = '[' + questions + ' questions left.]';

    var thing = prompt("Choose a type of object or thing. " + questionsLeft);

    // Create story variable with the base story plus inputted variables
    var story = "Once upon a time, " + name + " bravely " + adverb + " towards the mystical land of " + place + ", so that they might find the " + adjective + " cave of " + thing +"s.";

    // Alerting the user that they are done inputting words and asking if they are ready by asking them to click ok
    alert("All done. Ready for the story? Then click 'ok'!");

    // Writing the story to the page and concatenating the inputted words with the story sections
    document.write(story);
}
